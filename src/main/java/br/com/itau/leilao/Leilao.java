package br.com.itau.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    private List<Lance> lances;

    public Leilao() {
        this.lances = new ArrayList<Lance>();
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public void addLance(Lance lance){
        if(this.lances.size() > 0) {
            if (lance.getValorLance().compareTo(this.getLances().get(this.lances.size() - 1).getValorLance()) <= 0) {
                throw new RuntimeException("Lance precisa ser maior que o anterior");
            }
        }
        this.lances.add(lance);
    }

    public Lance retornaVencedor(){
        return this.lances.get(this.lances.size() - 1);
    }
}
