package br.com.itau.leilao;

import java.math.BigDecimal;

public class Lance {
    private Usuario usuario;
    private BigDecimal valorLance;

    public Lance(Usuario usuario, BigDecimal valorLance) {
        this.usuario = usuario;
        this.valorLance = valorLance;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public BigDecimal getValorLance() {
        return valorLance;
    }

    public void setValorLance(BigDecimal valorLance) {
        this.valorLance = valorLance;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj != null && obj instanceof Lance)
            return this.getValorLance().equals(((Lance) obj).getValorLance()) &&
                   this.getUsuario().equals(((Lance) obj).getUsuario());
        return false;
    }
}
