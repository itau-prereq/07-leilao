package br.com.itau.leilao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LanceTeste {
    private Usuario usuario;
    private Lance lance;
    private Leilao leilao;
    private Leiloeiro leiloeiro;

    private List<Usuario> listaUsuarioTeste = new ArrayList<Usuario>();
    private Lance lanceMaior;

    @BeforeEach
    public void setUp() {

        this.usuario = new Usuario(1, "Usuario teste");
        this.lance = new Lance(usuario, BigDecimal.valueOf(3.40));
        this.leilao = new Leilao();
        this.leiloeiro = new Leiloeiro("Nome Leiloeiro", this.leilao);
    }

     public void simulaLeilao(){
        for (int i = 1; i <= 10; i++){
            this.listaUsuarioTeste.add(new Usuario(i, "Teste " + i));
        }

        Random r = new Random();

        //inicia variavel zerada
        this.lanceMaior = new Lance(new Usuario(0, "teste"), BigDecimal.valueOf(0));

        // Simula leilao com 20 lances
        for (int i = 0; i < 20; i++){
            // Cria lance aleatorio
            Lance lanceRandom = new Lance(this.listaUsuarioTeste.get(r.nextInt(10)), BigDecimal.valueOf(r.nextInt(100000)/100.0).add(BigDecimal.valueOf(200)));

            // Guarda maior lance para comparar no assertEquals
            if (lanceRandom.getValorLance().compareTo(this.lanceMaior.getValorLance()) > 0){
                this.lanceMaior.setValorLance(lanceRandom.getValorLance());
                this.lanceMaior.setUsuario(lanceRandom.getUsuario());
            }

            // Tenta adicionar um lance no leilao
            try {
                this.leilao.addLance(lanceRandom);
            }
            catch(RuntimeException e){
                System.out.println(e.getMessage());
            }
        }
    }

    @Test
    public void TestaAdicionarLance(){
        this.lance.setValorLance(BigDecimal.valueOf(1201.01));
        this.leilao.addLance(this.lance);

        Assertions.assertTrue(this.leilao.getLances().contains(this.lance));
    }

    @Test
    public void TestaAdicionarLanceMenor(){
        simulaLeilao();
        Assertions.assertThrows(RuntimeException.class, () -> {this.leilao.addLance(this.lance);});
    }

    @Test
    public void TestaRetornaLanceMaior(){
        simulaLeilao();
        this.leilao = new Leilao();

        Assertions.assertEquals(this.lanceMaior, this.leiloeiro.getLeilao().retornaVencedor());
    }
}
